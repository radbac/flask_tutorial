from app.models import User, Message
from app import create_app, db
from app.models import Post
from datetime import datetime


def create_user(username, password, email):
    init_app(False)
    u = User.query.filter_by(username=username).first()

    if not u is None:
        db.session.delete(u)
        db.session.commit()

    u = User(username=username, email=email)

    u.set_password(password)
    db.session.add(u)
    db.session.commit()
    print('USER: %s created' % username)


def init_app(print_config):
    app = create_app()
    app_context = app.app_context()
    app_context.push()
    if print_config:
        for c in app.config.items():
            print(c)


def list_users():
    init_app(False)

    for u in User.query.all():
        print('%s %s %s' % (u.id, u.username, u.email))

def list_posts():
    init_app(False)
    for p in Post.query.all():
        print(p.user_id)


def elasticsearch_reindex():
    init_app(True)
    Post.reindex()


def recrate_db():
    init_app(False)
    db.session.remove()
    db.drop_all()
    db.create_all()


def fill_messages(u_from,u_to, cnt):
    init_app(False)
    user_from = User.query.filter_by(username=u_from).first()
    user_to = User.query.filter_by(username=u_to).first()

    for i in range(1, cnt + 1):
        mess = Message(sender_id=user_from.id,
                       recipient_id=user_to.id,
                       body='%s some text' % str(i),
                       timestamp=datetime.utcnow())
        db.session.add(mess)
        db.session.commit()


def main():
    # create_user(None,None,None)
    # list_users()
    # elasticsearch_reindex()
    # recrate_db()
    # create_user('radovan', 'radovan', 'radovan.bacovic@gmail.com')
    # create_user('susan', 'cat', 'sussan@gmail.com')
    # create_user('alice', 'dog', 'alice@gmail.com')
    # create_user('mama', 'mama123', 'mama@gmail.com')
    # create_user('tata', 'tata123', 'tata@gmail.com')
    # create_user('mile', 'mile', 'mile@radovan.com')
    # list_users()
    # list_posts()
    fill_messages('tata','susan',200)

if __name__ == "__main__":
    main()
