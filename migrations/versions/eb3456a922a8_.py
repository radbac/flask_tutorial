"""empty message

Revision ID: eb3456a922a8
Revises: 7aac34035197
Create Date: 2020-02-10 23:24:51.095263

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'eb3456a922a8'
down_revision = '7aac34035197'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
