"""empty message

Revision ID: b5d475d112ac
Revises: 2e1bb85181d6
Create Date: 2020-02-10 23:20:33.562825

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = 'b5d475d112ac'
down_revision = '2e1bb85181d6'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
