"""empty message

Revision ID: 2e1bb85181d6
Revises: 1d93a24481d6
Create Date: 2020-02-10 23:20:09.161748

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '2e1bb85181d6'
down_revision = '1d93a24481d6'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
