"""empty message

Revision ID: 8810152d9a3d
Revises: b5d475d112ac
Create Date: 2020-02-10 23:21:38.506858

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '8810152d9a3d'
down_revision = 'b5d475d112ac'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
