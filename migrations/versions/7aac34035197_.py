"""empty message

Revision ID: 7aac34035197
Revises: 8810152d9a3d
Create Date: 2020-02-10 23:24:44.954890

"""
from alembic import op
import sqlalchemy as sa


# revision identifiers, used by Alembic.
revision = '7aac34035197'
down_revision = '8810152d9a3d'
branch_labels = None
depends_on = None


def upgrade():
    pass


def downgrade():
    pass
